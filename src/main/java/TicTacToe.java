
import java.util.Scanner;

public class TicTacToe {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("welcome to OX Game");

        //make map
        char[][] map = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                map[i][j] = '-';
            }
        }

        // show map
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(map[i][j] + "    ");
            }
            System.out.println("\n");
        }
        System.out.println("turn O");

        //cheack trun
        char ox = 'O';
        int turn = 0;
        char winner = ' ';
        boolean win = false;

        while (win == false) {

            System.out.println("please input row,col : ");// input row and col

            int row = 0;
            int col = 0;
            //cheack space slot
            int slot = 0;
            while (slot == 0) {
                row = kb.nextInt();
                col = kb.nextInt();
                if(row>=4 || row <=0 || col >= 4 || col <=0){
                    System.out.println("ํYou're out of the map , Please input number 1-3 \n please input row , col : ");
                }else if (map[row - 1][col - 1] != '-') {
                    System.out.println("This slot have character \n please input row, col : ");
                } else {
                    slot = 1;
                }

            }

            map[row - 1][col - 1] = ox; //put O or X to map

            for (int i = 0; i < 3; i++) {  //create map
                for (int j = 0; j < 3; j++) {
                    System.out.print(map[i][j] + "    ");
                }
                System.out.println("\n");
            }
            //cheack who play next turn
            turn += 1;
            if (turn % 2 == 0) {
                ox = 'O';
            } else {
                ox = 'X';
            }

            //Win condition row
            for (int i = 0; i < 3; i++) {
                if (map[i][0] == map[i][1] && map[i][1] == map[i][2] && map[i][0] != '-') {
                    win = true;
                    winner = map[i][0];
                }
            }

            for (int j = 0; j < 3; j++) {
                if (map[0][j] == map[1][j] && map[1][j] == map[2][j] && map[0][j] != '-') {
                    win = true;
                    winner = map[0][j];
                }
            }

            if (map[0][0] == map[1][1] && map[1][1] == map[2][2] && map[0][0] != '-') {
                win = true;
                winner = map[0][0];
            }
            if (map[0][2] == map[1][1] && map[1][1] == map[2][0] && map[0][2] != '-') {
                win = true;
                winner = map[0][2];
            }

            if (turn == 9) {//draw and end
                System.out.println(">>>Draw<<<");
                break;
            }//next turn
            if (turn < 9 && win == false) {
                System.out.println("turn " + ox);
            } else {//win and end
                System.out.println(">>> " + winner + " WIN <<<");
                break;
            }

        }

    }
}
